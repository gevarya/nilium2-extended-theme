# README #

Extended Nilium2 color scheme based on ST2-Nil-Theme according to the modern sublime syntax manifesto. 
Created for SystemVerilog in mind but shall work on all languages and syntax

### References ###
https://github.com/nilium/st2-nil-theme
https://github.com/gwenzek/sublime-syntax-manifesto

## Screenshots ##
### TubNil extended ###
![nilium2-tubnil-extended.png](https://bitbucket.org/repo/jAgMMy/images/2420727959-nilium2-tubnil-extended.png)
### Big Duo extended ###
![nilium2-big-duo-extended.png](https://bitbucket.org/repo/jAgMMy/images/3583461745-nilium2-big-duo-extended.png)